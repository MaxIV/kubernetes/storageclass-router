# StorageClass Router

Admission controller to route PVC requests to the destination StorageClass in a random and round-robin manner.

The target use-case at MAX IV is to distribute PVCs created by StatefullSet between different storage backends (e.g. HA database with 3 replicas).
However the code is designed to provide flexibility for redirecting or rewrittng the StorageClasses by different algorithms.

## Example configuration

In the code snippet below there are several example routers defined to illustrate the possible approaches to StorageClass Router usage:

```yaml
config:
  loglevel: INFO
  routers:
    # use round-robin way of choosing between powerstore-kirk and powerstore-picard storage
    # based on PVC index suffix added by StatefulSet
    - name: powerstore-roundrobin
      type: index-based-roundrobin
      destination:
        static:
          - powerstore-kirk
          - powerstore-picard
    # use powerstore-kirk or powerstore-picard randomly
    - name: powerstore-any
      type: random
      destination:
        static:
          - powerstore-kirk
          - powerstore-picard
    # replace "mxn-nfs" with "ceph-filesystem" (random selection from list of 1 item)
    # do not create "mxn-nfs" StorageClass definition (it exists already)
    - name: mxn-nfs
      type: random
      createStorageClass: false
      destination:
        static:
          - ceph-filesystem
```
