{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "admission-controller.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "admission-controller.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "admission-controller.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "admission-controller.labels" -}}
helm.sh/chart: {{ include "admission-controller.chart" . }}
{{ include "admission-controller.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "admission-controller.selectorLabels" -}}
app.kubernetes.io/name: {{ include "admission-controller.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Name of the service account to use
*/}}
{{- define "admission-controller.serviceAccountName" -}}
{{- default (include "admission-controller.fullname" .) .Values.serviceAccount.name }}
{{- end }}

{{/*
Create admission service fullname with namespace as domain.
*/}}
{{- define "admission-controller.service.fullname" -}}
{{- default ( printf "%s.%s.svc" (include "admission-controller.fullname" .) .Release.Namespace ) -}}
{{- end -}}

{{/*
Generate TLS for admission-controller webhook
*/}}
{{- define "admission-controller.gen-certs" -}}
{{- $expiration := (.Values.config.tls.expiration | int) -}}
{{- $ca :=  genCA ( printf "%s-ca" .Release.Name ) $expiration -}}
{{- $altNames := list ( include "admission-controller.service.fullname" . ) -}}
{{- $cert := genSignedCert ( include "admission-controller.fullname" . ) nil $altNames $expiration $ca -}}
caCert: {{ b64enc $ca.Cert }}
clientCert: {{ b64enc $cert.Cert }}
clientKey: {{ b64enc $cert.Key }}
{{- end -}}

{{/*
Check query-based round-robin router algorithm is used somehere
*/}}
{{- define "storageclass-router.pvc-query" -}}
{{- range .Values.config.routers }}
{{- if eq .type "query-based-roundrobin" }}
true
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Conditional storageClass object creation
*/}}
{{- define "storageclass-router.createStorageClass" -}}
{{- if hasKey . "createStorageClass" }}
{{- if .createStorageClass }}
true
{{- end -}}
{{- else -}}
true
{{- end -}}
{{- end -}}
