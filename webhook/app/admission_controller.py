#!/usr/bin/env python3
import sys
import ssl
import os
import logging
import json
import base64
import jsonpatch
import random
from flask import Flask, request, jsonify
from kubernetes import client, config
from kubernetes.client.rest import ApiException

# logger
loglevel = os.environ.get('LOGLEVEL', 'DEBUG')
logger = logging.getLogger()
logger.setLevel(loglevel)
log_handler_stderr = logging.StreamHandler()
log_handler_stderr.setFormatter(
    logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
)
logger.addHandler(log_handler_stderr)

# storageclasses config 
router_config_f = os.environ.get('ROUTER_CONFIG', 'config.json')
router_config = {}
try:
    with open(router_config_f, 'r') as rc_fd:
        for c in json.load(rc_fd):
            if 'name' in c:
                router_config[c['name']] = c
            else:
                logger.error('Malformed storageclass configuration entry: %s', str(c))
except json.decoder.JSONDecodeError as e:
    logger.error('Failed to parse configuration file %s. %s', router_config_f, str(e))
    sys.exit(4)
except EnvironmentError as e:
    logger.error('Failed to open coniguration file %s. %s', router_config_f, str(e))
    sys.exit(4)

# init Kubernetes API connection
config.load_incluster_config()

#
# Kubernetes API Queries
#

def storage_classes(req_sc):
    api_instance = client.StorageV1Api()
    scs = []
    # get configuration
    sc_conf = router_config[req_sc]
    if 'destination' in sc_conf:
        if 'static' in sc_conf['destination']:
            if sc_conf['destination']['static']:
                sc_avail = []
                try:
                    api_response = api_instance.list_storage_class(pretty=False)
                    for sc in api_response.items:
                        sc_avail.append(sc.metadata.name)
                    logger.debug("Kubernetes has follwing storage classes configured: %s", ','.join(sc_avail))
                except ApiException as e:
                    logger.error("Exception when calling StorageV1Api->list_storage_class: %s\n" % e)
            for s in sc_conf['destination']['static']:
                if s in sc_avail:
                    scs.append(s)
                else:
                    logger.error('Destination StorageClass "%s" provided in static config does not exists', s)
        if 'selector' in sc_conf['destination']:
            if 'matchLabels' in sc_conf['destination']['selector']:
                s_list = []
                for k,v in sc_conf['destination']['selector']['matchLabels']:
                    s_list.append("{}={}".format(k,v))
                selector = ','.join(s_list)
                try:
                    api_response = api_instance.list_storage_class(label_selector=selector, pretty=False)
                    for sc in api_response.items:
                        logger.debug('Found storageclass %s matching the "%s" label(s)', sc.metadata.name, selector)
                        scs.append(sc.metadata.name)
                except ApiException as e:
                    logger.error("Exception when calling StorageV1Api->list_storage_class: %s\n" % e)
    if not scs:
        # backward compatibility fall-back (only if no conigured storageclassed)
        # use destination data from storage class annotation
        try:
            api_response = api_instance.read_storage_class(req_sc, pretty=False)
            annotations = api_response.metadata.annotations
            aname = 'storageclass-router.kubernetes.io/route-to'
            if aname in annotations:
                scs = json.loads(annotations[aname])
        except ApiException as e:
            logger.error("Exception when calling StorageV1Api->read_storage_class: %s\n" % e)
        if scs:
            logger.debug('Found storageclasses to route to in %s annotation: %s', aname, str(scs))

    if not scs:
        logger.error('There are no StorageClasses found to route request for "%s" StorageClass', req_sc)
    return scs

def pvc_storage_classes(ns, labels):
    api_instance = client.CoreV1Api()
    scs = {}
    selector = ''
    for k,v in labels.items():
        if selector:
            selector += ','
        selector += '{}={}'.format(k,v)
    try:
        api_response = api_instance.list_namespaced_persistent_volume_claim(namespace=ns, label_selector=selector, pretty=False)
        for pvc in api_response.items:
            sc = pvc.spec.storage_class_name
            logger.debug('Found PVC %s matching the "%s" labels. StorageClass: %s', pvc.metadata.name, selector, sc)
            if sc in scs:
                scs[sc] = scs[sc] + 1
            else:
                scs[sc] = 1
    except ApiException as e:
        logger.error("Exception when calling CoreV1Api->list_namespaced_persistent_volume_claim: %s\n" % e)
    logger.debug('PVC(s) StorageClasses: %s', str(scs))
    return scs

def int_checksum(buffer):
    nleft = len(buffer)
    sum = 0
    pos = 0
    while nleft > 1:
        sum = ord(buffer[pos]) * 256 + (ord(buffer[pos + 1]) + sum)
        pos = pos + 2
        nleft = nleft - 2
    if nleft == 1:
        sum = sum + ord(buffer[pos]) * 256

    sum = (sum >> 16) + (sum & 0xFFFF)
    sum += (sum >> 16)
    sum = (~sum & 0xFFFF)
    return int(sum)

def random_storageclass(req_sc):
    scs = storage_classes(req_sc)
    if not scs:
        return []
    logger.debug('Choosing from storageClasses: %s', str(scs))
    rsc = random.choice(scs)
    # return patch
    logger.info('Using randomly selected storage class %s for PVC', rsc)
    return [
      {"op": "replace", "path": "/spec/storageClassName", "value": rsc}
    ]

def rrobin_storageclass_index(req_sc, ns, pvc_name):
    scs = storage_classes(req_sc)
    if not scs:
        return []
    logger.debug('Choosing from storageClasses: %s', str(scs))
    # get PVC index
    rsc = None
    try:
        pvc_index = int(pvc_name.split('-')[-1])
        logger.debug('PVC sequence index is %s', pvc_index)
        start_index = int_checksum(ns + '-'.join(pvc_name.split('-')[:-1])) % len(scs)
        logger.debug('Checksum-based start index is %s', start_index)
        sc_index = ( start_index + pvc_index ) % len(scs)
        rsc = scs[sc_index]
    except ValueError as e:
        logger.debug('PVC name %s does not have index suffix. Parse error: %s', pvc_name, str(e))
        sc_index = int_checksum(ns + pvc_name) % len(scs)
        logger.debug('Using checksum-based index %s', sc_index)
        rsc = scs[sc_index]
    # return patch
    if rsc is None:
        return []
    logger.info('Using storage class %s for PVC %s', rsc, pvc_name)
    return [
      {"op": "replace", "path": "/spec/storageClassName", "value": rsc}
    ]


def rrobin_storageclass_query(req_sc, ns, pvc_name, pvc_labels):
    scs = storage_classes(req_sc)
    if not scs:
        return []
    pvcscs = pvc_storage_classes(ns, pvc_labels)
    # check for available but not used
    for s in scs:
        if s not in pvcscs:
            pvcscs[s] = 0
    # check for used but not available for routing
    dk = []
    for k in pvcscs.keys():
        if k not in scs:
            dk.append(k)
    for k in dk:
        del pvcscs[k]
    logger.debug('Choosing from storageClasses: %s', str(pvcscs))
    # find minimal usage
    rr_sc = []
    min_v = 999999999
    for k, v in pvcscs.items():
        if v < min_v:
            min_v = v
            rr_sc = [ k ]
        elif v == min_v:
            rr_sc.append(k)
    # use random among same
    rsc = random.choice(rr_sc)
    # return patch
    logger.info('Using storage class %s for PVC %s', rsc, pvc_name)
    return [
      {"op": "replace", "path": "/spec/storageClassName", "value": rsc}
    ]

#
# Admission Controller Webhook
#

def return_empty_response(uid):
    return jsonify({"apiVersion": "admission.k8s.io/v1",
      "kind": "AdmissionReview",
      "response": {
         "uid": uid,
         "allowed": True,
       }
    })

# init flask app
admission_controller = Flask(__name__)
if loglevel == 'DEBUG':
    admission_controller.config["DEBUG"] = True

# webhook route
@admission_controller.route('/mutate/pvcs', methods=['POST'])
def pod_webhook_mutate():
    request_info = request.get_json()
    logger.debug("Request JSON: %s", json.dumps(request_info))
    uid = request_info['request']['uid']
    # storageClass
    sc = ''
    if 'storageClassName' in request_info['request']['object']['spec']:
        sc = request_info['request']['object']['spec']['storageClassName']
    # PVC labels
    pvc_labels = {}
    if 'labels' in request_info['request']['object']['metadata']:
        pvc_labels = request_info['request']['object']['metadata']['labels']
    # PVC name and namespace
    pvc_name = request_info['request']['object']['metadata']['name']
    namespace = request_info['request']['object']['metadata']['namespace']
    # skip storageclasses we don't handle
    if not sc:
        logger.info('Controller does not handle empty storageClass. Skipping request')
        return return_empty_response(uid)
    elif sc not in router_config:
        logger.info('Controller does not handle storageClass "%s". Skipping.', sc)
        return return_empty_response(uid)

    # create patch depend on router type
    patches = []
    sc_conf = router_config[sc]
    if sc_conf['type'] == 'random':
        patches = random_storageclass(sc)
    elif sc_conf['type'] == 'query-based-roundrobin':
        patches = rrobin_storageclass_query(sc, namespace, pvc_name, pvc_labels)
    elif sc_conf['type'] == 'index-based-roundrobin':
        patches = rrobin_storageclass_index(sc, namespace, pvc_name)
    else:
        logger.error('Unknown router storageClass type "%s" for "%s". Ignoring request.', sc_conf['type'], sc)
        return return_empty_response(uid)

    if not patches:
        logger.info('Skipping storageClass %s routing for PVC %s.', sc, pvc_name)
        return return_empty_response(uid)

    logger.debug("Patching PVC %s with: %s", pvc_name, json.dumps(patches))
    try:
        base64_patch = base64.b64encode(jsonpatch.JsonPatch(patches).to_string().encode("utf-8")).decode("utf-8")
    except jsonpatch.InvalidJsonPatch as e:
        logger.error("Invalid Json Patch: %s", str(e))
        return return_empty_response(uid)

    return jsonify({"apiVersion": "admission.k8s.io/v1",
      "kind": "AdmissionReview",
      "response": {
         "uid": uid,
         "allowed": True,
         "patchType": "JSONPatch",
         "patch": base64_patch
      }
    })

# healthcheck route
@admission_controller.route('/healthz', methods=['GET'])
def deployment_healthz():
    return jsonify('OK')

# gunicorn defaults
application = admission_controller

# run app
if __name__ == '__main__':
    # webhook web service setup
    tls_cert = os.environ.get('TLS_CERT', None)
    tls_key = os.environ.get('TLS_KEY', None)
    https_port = int(os.environ.get('HTTPS_PORT', 8443))

    # config checks
    if tls_cert is None or tls_key is None:
        logger.error("TLS key/cert are not defined. Exiting.")
        sys.exit(1)
    admission_controller.run(host='0.0.0.0', port=https_port, ssl_context=(tls_cert, tls_key))
