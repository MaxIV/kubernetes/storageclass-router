import sys
import os

workers = int(os.environ.get('GUNICORN_WORKERS', '1'))

loglevel = os.environ.get('LOGLEVEL', 'DEBUG').lower()
accesslog = '-'

certfile = os.environ.get('TLS_CERT', None)
keyfile = os.environ.get('TLS_KEY', None)
#if certfile is None or keyfile is None:
#    print("TLS key/cert are not defined. Exiting.")
#    sys.exit(1)

bind = '0.0.0.0:{}'.format(os.environ.get('HTTPS_PORT', '8443'))

def worker_int(worker):
    print('Terminating because of worker failure')
    sys.exit(1)