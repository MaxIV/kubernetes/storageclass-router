# StorageClass Router

[![Container Image](https://img.shields.io/badge/Container%20Image-quay.io-green.svg)](https://quay.io/repository/maxiv/storageclass-router) [![Artifact Hub](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/storageclass-router)](https://artifacthub.io/packages/search?repo=storageclass-router)

Admission controller to route PVC requests to the destination StorageClass in a random and round-robin manner.

Installation is done via Helm Chart. Several routers can be defined in the `values.yaml`. See more details in the [Helm Chart README](./helm/README.md).
